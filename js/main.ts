/* --- pre loader --- */
$guelfi(document).ready(function () {
    //Preloader
    $guelfi(window).load(function () {
        preloaderFadeOutTime = 500;
        function hidePreloader() {
            var preloader = $guelfi('.spinner-wrapper');
            preloader.fadeOut(preloaderFadeOutTime);
        }
        hidePreloader();
    });
});

/* --- Jquery ---*/
/*funcao logo fadeIn*/
$guelfi(function () {
    /* --- toggle logo section --- */
    $guelfi('section.documentary').bind('mouseover', function () {
        $guelfi('#logo').fadeIn('slow')
    })
    $guelfi('section.introduction').bind('mouseover', function () {
        $guelfi('#logo').hide('slow')
    })

    /* --- hide elementos inuteis --- */
    $guelfi('#targetDetails').hide()
    $guelfi('#targetDownload').hide()
    $guelfi('#targetFollow').hide()
    $guelfi('#footer_video').hide()
    $guelfi('#targetParceiros').hide()
    $guelfi('#targetDetailsToForm').hide()
    $guelfi('#forgetThisThingNow').hide()
    $guelfi('#forgetThisThingNow1').hide()
    $guelfi('#forgetThisThingNow2').hide()
    $guelfi('#forgetThisThingNow3').hide()
    $guelfi('#forgetThisThingNow4').hide()
    $guelfi('#forgetThisThingNow5').hide()
    $guelfi('#forgetThisThingNow6').hide()
    $guelfi('#hideHideHideAll').hide()
    $guelfi('#forgetNowRight').hide()
    $guelfi('#forgetNowRight1').hide()
    $guelfi('#someAgora').hide()
    $guelfi('#someSomeSome').hide()
    $guelfi('#targetTimeline').hide()
    $guelfi('li.button_about').hide()

    /* --- change propriets or styles --- */
    $guelfi('#targetInstagram').html('<img src="assets/images/blog.png">')
    $guelfi('#alvo').css('color', 'red')
    $guelfi('#changeColor').css('color', 'a1a1a1')
    $guelfi('button').css('text-transform', 'uppercase')
    $guelfi('input text-area').css('font-family', 'futuraMd')
    $guelfi('#botaoBoo').css('color', 'blue')
    $guelfi('.step-background').css('background', 'white')
    $guelfi('#targetInstagram').html('<img src="assets/images/blog.png"></img>')
    $guelfi("li.button_pictures").css('opacity', '0')
    $guelfi('#selectTarget').on('click', function () {
        $guelfi('#selectTarget').css('color', '000')
    })

    /* --- hover buttons --- */
    $guelfi("button").hover(function () {
        $guelfi(this).toggleClass("hoverButton", 20000)
    })
    $guelfi("#buttonBranco").hover(function () {
        $guelfi(this).toggleClass("hoverButtonBranco", 20000)
    })
    $guelfi("li").hover(function () {
        $guelfi(this).toggleClass("active", 20000)
    })

    /* --- simula click menu --*/
    $guelfi("#clickSolicitar").bind('click', function () {
        $guelfi("li.voice_commands").click()
    })
    $guelfi("#clickSolicitar1").bind('click', function () {
        $guelfi("li.voice_commands").click()
    })
    $guelfi('#clickAlbum').bind('click', function () {
        $guelfi("li.button_pictures").click()
    })

    /* --- clicks menu --- */
    $guelfi('#clickHome').bind('click', function () {
        $guelfi('li.introduction').click()
    })


    /* --- form middle --- */
    /* --- Input nome ---*/
    $guelfi('#nomeMiddle').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#nomeMiddle').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })
    /* --- Input E-mail --- */
    $guelfi('#emailMiddle').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#emailMiddle').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })
    /* --- Input Telefone --- */
    $guelfi('#telefoneMiddle').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#telefoneMiddle').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })
    $guelfi('#telefoneMiddle').mask('(00) 00000-0000')
    /* --- Input Nome do Aniversariante --- */
    $guelfi('#nomeAniversarianteMiddle').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#nomeAniversarianteMiddle').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })
    /* --- Input Idade Aniversariante --- */
    $guelfi('#idadeAniversarianteMiddle').focus(function () {
        $guelfi('#idadeAniversarianteMiddle').mask('00')
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#idadeAniversarianteMiddle').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })
    /* --- Input Date --- */
    $guelfi('#dataFestaMiddle').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
        /*$guelfi(this).attr('type','date')*/
    })
    $guelfi('#dataFestaMiddle').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
        /*$guelfi(this).attr('type', 'text')*/
    })
    $guelfi('#dataFestaMiddle').mask('00/00/0000')

    /* --- Input Periodo --- */
    $guelfi('#periodoMiddle').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#periodoMiddle').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })
    /* --- Input Numero Convidados --- */
    $guelfi('#numeroConvidados').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
        $guelfi(this).mask('000')
    })
    $guelfi('#numeroConvidados').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })
    /* --- Input Mensagem --- */
    $guelfi('#mensagemMiddle').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#mensagemMiddle').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })

    /* --- Validacoes Form Middle --- */
    $guelfi('#numeroConvidados').bind('click', function () {
        $guelfi(this).attr('value', '50')
    })
    /* --- numero min convidados --- */
    $guelfi("#numeroConvidados").bind('touchend click', function () {
        let valor = $guelfi(this).val()
        if (valor < 50) {
            $guelfi('#numeroConvidados').attr('value', '50')
            alert('Numero minimo de convidados 50')
        } else {
            console.log('else')
            $guelfi('#numeroConvidados').attr('value', valor)
        }
    })
    /* --- numero max convidados --- */
    $guelfi("#numeroConvidados").bind('touchend click', function () {
        let valor = $guelfi(this).val()
        if (valor > 500) {
            alert('Numero minimo de maximo 500')
            $guelfi('#numeroConvidados').html('<input placeholder="Nº de Convidados" id="numeroConvidados" value="" type="number" maxlength="3" minlength="2" class="entrelinhas formatacao-input-middle">')
        } else {
            console.log('else')
            $guelfi('#numeroConvidados').attr('value', valor)
        }
    })

    $guelfi("#numeroConvidados").bind('focusout', function () {
        let valor = $guelfi(this).val()
        if (valor < 50) {
            $guelfi('#numeroConvidados').attr('value', '50')
        }
    })

    /* --- Form Footer --- */
    /* --- input nome --- */
    $guelfi('#nome').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#nome').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })

    /* --- input telefone --- */
    $guelfi('#email').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#email').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })

    /* --- input telefone --- */
    $guelfi('#telefoneCelular').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#telefoneCelular').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })

    /* --- text area mensagem --- */
    $guelfi('#mensagem').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#mensagem').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })

    /* --- insira seu email footer --- */
    $guelfi('#insiraSeuEmail').focus(function () {
        $guelfi(this).css('border-color', '#ffcb4f')
    })
    $guelfi('#insiraSeuEmail').focusout(function () {
        $guelfi(this).css('border-color', '#f0f0f0')
    })

    /* --- slider atributos --- */
    /* nao alterar a versao do jquery para este slide */
    $('#autoplay').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2500,
        dots: true
    })

    /* menu via parametro */

    // Parsiar URL via parametro
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    // func pega parametro
    var dynamicContent = getParameterByName('guelfi')

    // verifica parametro
    if (dynamicContent == 'espacoBoo') {
        setTimeout(() => {
            let buttonOrcamento = $('.introduction');
            buttonOrcamento.click().off();
        }, 15000);
    }
    else if (dynamicContent == 'assistaAoNossoVideo') {
        setTimeout(() => {
            let buttonOrcamento = $('.documentary');
            buttonOrcamento.click().off();
            $('.welcome').hide();
        }, 15000);
    }
    else if (dynamicContent == 'sobreBuffet') {
        setTimeout(() => {
            let buttonOrcamento = $('.project');
            buttonOrcamento.click().off();
            $('.welcome').hide();
        }, 15000);
    }
    else if (dynamicContent == 'orcamento') {
        setTimeout(() => {
            let buttonOrcamento = $('.voice_commands');
            buttonOrcamento.click().off();
            $('.welcome').hide();
        }, 15000);
    }
    else if (dynamicContent == 'galeria') {
        setTimeout(() => {
            let buttonOrcamento = $('.writers');
            buttonOrcamento.click().off();
            $('.welcome').hide();
        }, 15000);
    }
    else if (dynamicContent == 'blog') {
        setTimeout(() => {
            let buttonOrcamento = $('.instagram');
            buttonOrcamento.click().off();
            $('.welcome').hide();
        }, 15000);
    }
    else (dynamicContent == '') {
        console.log('parametro invalido');
    }
})

