$(function () {


    /*--- hover outras festas --- */

    /*--- 1 --- */
    $(".backgroundOutrasFestas1").hover(function () {
        $(this).addClass('backgroundOutrasFestas1Hover');
        $('#targetButtonOutrasFestas1').addClass('botao-outras-festas')

    }, function () {
        $(this).removeClass('backgroundOutrasFestas1Hover');
        $('#targetButtonOutrasFestas1').removeClass('botao-outras-festas')
    });

    /*--- 2 --- */
    $(".backgroundOutrasFestas2").hover(function () {
        $(this).addClass('backgroundOutrasFestas2Hover');
        $('#targetButtonOutrasFestas2').addClass('botao-outras-festas')

    }, function () {
        $('#targetButtonOutrasFestas2').removeClass('botao-outras-festas')
        $(this).removeClass('backgroundOutrasFestas2Hover');
    });

    /*--- 3 --- */
    $(".backgroundOutrasFestas3").hover(function () {
        $('#targetButtonOutrasFestas3').addClass('botao-outras-festas')
        $(this).addClass('backgroundOutrasFestas3Hover');

    }, function () {
        $('#targetButtonOutrasFestas3').removeClass('botao-outras-festas')
        $(this).removeClass('backgroundOutrasFestas3Hover');
    });

    /*--- 4 --- */
    $(".backgroundOutrasFestas4").hover(function () {
        $('#targetButtonOutrasFestas4').addClass('botao-outras-festas')
        $(this).addClass('backgroundOutrasFestas4Hover');

    }, function () {
        $('#targetButtonOutrasFestas4').removeClass('botao-outras-festas')
        $(this).removeClass('backgroundOutrasFestas4Hover');
    });

    /*--- 5 --- */
    $(".backgroundOutrasFestas5").hover(function () {
        $('#targetButtonOutrasFestas5').addClass('botao-outras-festas')
        $(this).addClass('backgroundOutrasFestas5Hover');

    }, function () {
        $('#targetButtonOutrasFestas5').removeClass('botao-outras-festas')
        $(this).removeClass('backgroundOutrasFestas5Hover');
    });




    /*--- hover galeria --- */

    /*--- 1 --- */
    $("#targetHoverButton").hover(function () {
        $(this).addClass('efeitoBackground')
        $('#targetButton').addClass('botao-galeria');
        $('#logo1').addClass('efeitoButtonHover');

    }, function () {
        $(this).removeClass('efeitoBackground')
        $('#targetButton').removeClass('botao-galeria');
        $('#logo1').removeClass('efeitoButtonHover');
    });
    /*--- 2 --- */
    $("#targetHoverButton1").hover(function () {
        $(this).addClass('efeitoBackground')
        $('#targetButton1').addClass('botao-galeria');
        $('#logo2').addClass('efeitoButtonHover');
    }, function () {
        $(this).removeClass('efeitoBackground')
        $('#targetButton1').removeClass('botao-galeria');
        $('#logo2').removeClass('efeitoButtonHover');
    });
    /*--- 3 --- */
    $("#targetHoverButton2").hover(function () {
        $(this).addClass('efeitoBackground')
        $('#targetButton2').addClass('botao-galeria');
        $('#logo3').addClass('efeitoButtonHover');
    }, function () {
        $(this).removeClass('efeitoBackground')
        $('#targetButton2').removeClass('botao-galeria');
        $('#logo3').removeClass('efeitoButtonHover');
    });
    /*--- 4 --- */
    $("#targetHoverButton3").hover(function () {
        $(this).addClass('efeitoBackground')
        $('#targetButton3').addClass('botao-galeria');
        $('#logo4').addClass('efeitoButtonHover');
    }, function () {
        $(this).removeClass('efeitoBackground')
        $('#targetButton3').removeClass('botao-galeria');
        $('#logo4').removeClass('efeitoButtonHover');
    });

    /* --- cardapio dinamico 2 --- */
    $('#targetClickAltura').click(function () {
        $('#targetAltura').toggleClass('setTamanho'), 1000;
        $('#targetBackground').toggleClass('setTamanhoSection'), 1000;
    });
    $('#targetClickAlturaEntrada').click(function () {
        $('#targetAlturaEntrada').toggleClass('setTamanho', 1000);
        $('#targetBackground').toggleClass('setTamanhoSection', 1000);
    });

    $('#targetClickAlturaFingerFoods').click(function () {
        $('#targetAlturaFingerFoods').toggleClass('setTamanho', 1000);
        $('#targetBackground').toggleClass('setTamanhoSection', 1000);
    });

    $('#targetClickAlturaSalgados').click(function () {
        $('#targetAlturaSalgados').toggleClass('setTamanho', 1000);
        $('#targetBackground').toggleClass('setTamanhoSection', 1000);
    });

    $('#targetClickAlturaMenuDegustacao').click(function () {
        $('#targetAlturaMenuDegustacao').toggleClass('setTamanho', 1000);
        $('#targetBackground').toggleClass('setTamanhoSection', 1000);
    });

    $('#targetClickAlturaDocinhos').click(function () {
        $('#targetAlturaDocinhos').toggleClass('setTamanho', 1000);
        $('#targetBackground').toggleClass('setTamanhoSection', 1000);
    });

    $('#targetClickAlturaBebidas').click(function () {
        $('#targetAlturaBebidas').toggleClass('setTamanho', 1000);
        $('#targetBackground').toggleClass('setTamanhoSection', 1000);
    });

    $('#targetClickAlturaBolos').click(function () {
        $('#targetAlturaBolos').toggleClass('setTamanho', 1000);
        $('#targetBackground').toggleClass('setTamanhoSection', 1000);
    });

    /* --- new jquery --- */
    $("#paragrafoSuaEmpresa").hover(function () {
        $(this).toggleClass("paragrafoSuaEmpresa", 1000)
    })

    $("#paragrafoSuaEmpresa1").hover(function () {
        $(this).toggleClass("paragrafoSuaEmpresa", 1000)
    })

    $("#paragrafoSuaEmpresa2").hover(function () {
        $(this).toggleClass("paragrafoSuaEmpresa", 1000)
    })

    $("#paragrafoSuaEmpresa3").hover(function () {
        $(this).toggleClass("paragrafoSuaEmpresa", 1000)
    })

    /* waypoint sua-festa-tema */
    let waypointSuaFestaTema = new Waypoint({
        element: document.getElementById('sua-festa-tema'),
        handler: () => {
            /* logo superior fixo */
            let logoSuperiorEsquerda = document.getElementById('logo')
            logoSuperiorEsquerda.setAttribute("src", "assets/images/header/logo-colorido.png")

            let menuEsq = document.getElementById('changeIconMenu')
            if (stats == 0) {
                stats = 1
                menuEsq.setAttribute("src", "assets/images/festa-corporativa/menu-preto.png")
            } else if (stats == 1) {
                stats = 0
                menuEsq.setAttribute("src", "assets/images/festa-corporativa/menu.png")
            }          
        },
        offset: 20
    })
    let stats = 0;

    /*waypoint cardapio */
    let waypoint = new Waypoint({
        element: document.getElementById('cardapio'),
        handler: () => {

            let menuEsq = document.getElementById('changeIconMenu')
            if (statsCard == 0) {
                statsCard = 1
                menuEsq.setAttribute("src", "assets/images/festa-corporativa/menu.png")
            } else if (statsCard == 1) {
                statsCard = 0
                menuEsq.setAttribute("src", "assets/images/festa-corporativa/menu-preto.png")
        },
        offset: 20
    })
    let statsCard = 0;

    /*waypoint formulario */
    let waypoint = new Waypoint({
        element: document.getElementById('formulario'),
        handler: () => {

            let menuEsq = document.getElementById('changeIconMenu')
            if (statsForm == 0) {
                statsForm = 1
                menuEsq.setAttribute("src", "assets/images/festa-corporativa/menu-preto.png")
            } else if (statsForm == 1) {
                statsForm = 0
                menuEsq.setAttribute("src", "assets/images/festa-corporativa/menu.png")
            } 
        },
        offset: 20
    })
    let statsForm = 0;

    /*waypoint galeria */
    let waypoint = new Waypoint({
        element: document.getElementById('galeria'),
        handler: () => {
            let menuEsq = document.getElementById('changeIconMenu')
            if (statsGal == 0) {
                statsGal = 1
                menuEsq.setAttribute("src", "assets/images/festa-corporativa/menu.png")
            } else if (statsGal == 1) {
                statsGal = 0
                menuEsq.setAttribute("src", "assets/images/festa-corporativa/menu-preto.png")
        },
        offset: 20
    })
    let statsGal = 0;

    /*waypoint footer */
    let waypoint = new Waypoint({
        element: document.getElementById('footer'),
        handler: () => {
            let menuEsq = document.getElementById('changeIconMenu')
            if (statsFooter == 0) {
                statsFooter = 1
                menuEsq.setAttribute("src", "assets/images/festa-corporativa/menu-preto.png")
            } else if (statsFooter == 1) {
                statsFooter = 0
                menuEsq.setAttribute("src", "assets/images/festa-corporativa/menu.png")
            } 
        },
        offset: 20
    })
    let statsFooter = 0;


    /* --- form ---*/
    /* --- form middle --- */
    /* --- Input nome ---*/
    $('#nomeMiddle').focus(function () {
        $(this).css('border-color', '#ffcb4f')
    })
    $('#nomeMiddle').focusout(function () {
        $(this).css('border-color', '#f0f0f0')
    })
    /* --- Input E-mail --- */
    $('#emailMiddle').focus(function () {
        $(this).css('border-color', '#ffcb4f')
    })
    $('#emailMiddle').focusout(function () {
        $(this).css('border-color', '#f0f0f0')
    })
    /* --- Input Telefone --- */
    $('#telefoneMiddle').focus(function () {
        $(this).css('border-color', '#ffcb4f')
    })
    $('#telefoneMiddle').focusout(function () {
        $(this).css('border-color', '#f0f0f0')
    })
    /* --- Input Nome do Aniversariante --- */
    $('#nomeAniversarianteMiddle').focus(function () {
        $(this).css('border-color', '#ffcb4f')
    })
    $('#nomeAniversarianteMiddle').focusout(function () {
        $(this).css('border-color', '#f0f0f0')
    })
    /* --- Input Idade Aniversariante --- */
    $('#idadeAniversarianteMiddle').focus(function () {
        $(this).css('border-color', '#ffcb4f')
    })
    $('#idadeAniversarianteMiddle').focusout(function () {
        $(this).css('border-color', '#f0f0f0')
    })
    /* --- Input Date --- */
    $('#dataFestaMiddle').focus(function () {
        $(this).css('border-color', '#ffcb4f')
        /*$(this).attr('type','date')*/
    })
    $('#dataFestaMiddle').focusout(function () {
        $(this).css('border-color', '#f0f0f0')
        /*$(this).attr('type', 'text')*/
    })

    /* --- Input Periodo --- */
    $('#periodoMiddle').focus(function () {
        $(this).css('border-color', '#ffcb4f')
    })
    $('#periodoMiddle').focusout(function () {
        $(this).css('border-color', '#f0f0f0')
    })
    $('#selectTarget').on('click', function () {
        $('#selectTarget').css('color', '000')
    })
    /* --- Input Numero Convidados --- */
    $('#numeroConvidados').focus(function () {
        $(this).css('border-color', '#ffcb4f')
    })
    $('#numeroConvidados').focusout(function () {
        $(this).css('border-color', '#f0f0f0')
    })
    /* --- Input Mensagem --- */
    $('#mensagemMiddle').focus(function () {
        $(this).css('border-color', '#ffcb4f')
    })
    $('#mensagemMiddle').focusout(function () {
        $(this).css('border-color', '#f0f0f0')
    })

    /* --- Validacoes Form Middle --- */
    $('#numeroConvidados').bind('click', function () {
        $(this).attr('value', '50')
    })
    /* --- numero min convidados --- */
    $("#numeroConvidados").bind('touchend click', function () {
        let valor = $(this).val()
        if (valor < 50) {
            $('#numeroConvidados').attr('value', '50')
        } else {
            console.log('else')
            $('#numeroConvidados').attr('value', valor)
        }
    })
    /* --- numero max convidados --- */
    $("#numeroConvidados").bind('touchend click', function () {
        let valor = $(this).val()
        if (valor > 500) {
            $('#numeroConvidados').html('<input placeholder="Nº de Convidados" id="numeroConvidados" value="" type="number" maxlength="3" minlength="2" class="entrelinhas formatacao-input-middle">')
        } else {
            console.log('else')
            $('#numeroConvidados').attr('value', valor)
        }
    })

    $("#numeroConvidados").bind('focusout', function () {
        let valor = $(this).val()
        if (valor < 50) {
            $('#numeroConvidados').attr('value', '50')
        }
    })

    /* --- select efeito --- */
    $('#selectTarget').on('click', function () {
        $('#selectTarget').css('color', '000')
    })

    /* --- jquery mask --- */
    $('#telefoneMiddle').mask('(00) 00000-0000');
    $('#dataFestaMiddle').mask('00/00/0000');

    /* --- hover buttons --- */
    $("button").hover(function () {
        $(this).toggleClass("hoverButton")
    })
    $("#buttonBranco").hover(function () {
        $(this).toggleClass("hoverButtonBranco")
    })

    /* --- scrool suave * ---*/
    $('#clickDesca').click(function (e) {
        e.preventDefault();
        let id = $(this).attr("href");
        let targetOffset = $(id).offset().top;
        $("html, body").animate({ scrollTop: targetOffset }, 2000);
    });

    /* --- insira seu email footer --- */
    $('#insiraSeuEmail').focus(function () {
        $(this).css('border-color', '#ffcb4f')
    })
    $('#insiraSeuEmail').focusout(function () {
        $(this).css('border-color', '#f0f0f0')
    })

    $('#insiraSeuEmail').focus(function () {
        $(this).css('border-color', '#ffcb4f')
    })
    $('#insiraSeuEmail').focusout(function () {
        $(this).css('border-color', '#f0f0f0')
    })

    /* --- form --- */
    /* nome */
    $('.nome-footer-form').focus(function () {
        $(this).css('border-color', '#ffcb4f')
        $(this).focusout(function () {
            $(this).css('border-color', '#f0f0f0')
        })
    })
    /* email */
    $('.email-footer-form').focus(function () {
        $(this).css('border-color', '#ffcb4f')
        $(this).focusout(function () {
            $(this).css('border-color', '#f0f0f0')
        })
    })
    /* telefone */
    $('.telefone-footer-form').focus(function () {
        $(this).css('border-color', '#ffcb4f')
        $(this).focusout(function () {
            $(this).css('border-color', '#f0f0f0')
        })
    })

    /* text area mensagem */
    $('.mensagem-footer-form').focus(function () {
        $(this).css('border-color', '#ffcb4f')
        $(this).focusout(function () {
            $(this).css('border-color', '#f0f0f0')
        })
    })

    /* Menu */

    //Using Vanilla JS
    document.querySelector(".hamburguer").addEventListener("click", function () {
        document.querySelector(".full-menu").classList.toggle("active");
        document.querySelector(".hamburguer").classList.toggle("close-hamburguer");
    });


    $('#changeIconMenu').bind('click', function () {
        let avaliador = $(this).attr('src')
        if (avaliador == 'assets/images/festa-corporativa/menu.png') {
            $(this).attr('src', 'assets/images/website/menu/button_close_black.png')
        } else if (avaliador == 'assets/images/website/menu/button_close_black.png') {
            $(this).attr('src', 'assets/images/festa-corporativa/menu.png')
        }
    })
    /* aumentar ou diminuir tamanho da section no menu */

    /*Entrada*/
    $("#targetClickAlturaEntrada").click(() => {
        let valor: int = $('.resultadoAltura').height();
        //console.log(valor)
        //console.log(state1)
        if (state1 == 0) {
            state1 = 1;
            valor = valor + 550;
            //console.log(`valor apos adisao ${valor}`)
        } else if (state1 == 1) {
            state1 = 0;
            valor = valor - 550;
            //console.log(`valor apos subtracao ${valor}`)
        }
        $('#targetBackground').css('height', valor)
    });
    let state1: boolean = 0;

    /*FingerFoods*/
    $('#targetClickAlturaFingerFoods').click(() => {
        let valor: int = $('.resultadoAltura').height();
        if (state2 == 0) {
            state2 = 1;
            valor = valor + 550;
        } else if (state2 == 1) {
            state2 = 0;
            valor = valor - 550;
        }
        $('#targetBackground').css('height', valor)
    });
    let state2: boolean = 0;

    /*Salgados*/
    $('#targetClickAlturaSalgados').click(() => {
        let valor: int = $('.resultadoAltura').height();
        if (state3 == 0) {
            state3 = 1;
            valor = valor + 550;
        } else if (state3 == 1) {
            state3 = 0;
            valor = valor - 550;
        }
        $('#targetBackground').css('height', valor)
    });
    let state3: boolean = 0;

    /*Piquenique*/
    $('#targetClickAltura').click(() => {
        let valor: int = $('.resultadoAltura').height();
        if (state4 == 0) {
            state4 = 1;
            valor = valor + 550;
        } else if (state4 == 1) {
            state4 = 0;
            valor = valor - 550;
        }
        $('#targetBackground').css('height', valor)
    });
    let state4: boolean = 0;

    /*Degustação*/
    $('#targetClickAlturaMenuDegustacao').click(() => {
        let valor: int = $('.resultadoAltura').height();
        if (state5 == 0) {
            state5 = 1;
            valor = valor + 550;
        } else if (state5 == 1) {
            state5 = 0;
            valor = valor - 550;
        }
        $('#targetBackground').css('height', valor)
    });
    let state5: boolean = 0;

    /*Docinhos*/
    $('#targetClickAlturaDocinhos').click(() => {
        let valor: int = $('.resultadoAltura').height();
        if (state6 == 0) {
            state6 = 1;
            valor = valor + 550;
        } else if (state6 == 1) {
            state6 = 0;
            valor = valor - 550;
        }
        $('#targetBackground').css('height', valor)
    });
    let state6: boolean = 0;

    /*Bebidas*/
    $('#targetClickAlturaBebidas').click(() => {
        let valor: int = $('.resultadoAltura').height();
        if (state7 == 0) {
            state7 = 1;
            valor = valor + 550;
        } else if (state7 == 1) {
            state7 = 0;
            valor = valor - 550;
        }
        $('#targetBackground').css('height', valor)
    });
    let state7: boolean = 0;

    /*Bolos*/
    $('#targetClickAlturaBolos').click(() => {
        let valor: int = $('.resultadoAltura').height();
        if (state8 == 0) {
            state8 = 1;
            valor = valor + 550;
        } else if (state8 == 1) {
            state8 = 0;
            valor = valor - 550;
        }
        $('#targetBackground').css('height', valor)
    });
    let state8: boolean = 0;


})


